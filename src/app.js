const path = require('path');
const express = require('express');
const hbs = require('hbs');
const { title } = require('process');
const log = console.log;
const geocode = require('./utils/geocode.js');
const forecast = require('./utils/forecast.js');


const app = express();
const port = process.env.PORT || 3000;

// Define paths for express config....
const publicDir = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');

// Set up handlerbars engine and views location... 
app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath)

// line 12 only needfull or worthy when we want to serve only static pages to the apps in public folder.......
// still it is necessary for accessing css and js files from public folder....
app.use(express.static(publicDir));

app.get('', (req, res) => {
    res.render('index', {
        title: 'Weather App',
        name: 'John Doe'
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        name: 'Sam curran',
        title: 'About me',
        img: '/images/robot.png'
    });
});

app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Who we are',
        name: 'John Doe',
        msg: 'this is the help message, Please go through it completely......'
    });
});

app.get('/weather', (req, res) => {
    if (req.query.address) {
        geocode(req.query.address, (error, { location, latitude, longitude } = {}) => {
            if (error) {
                return res.send({ error });
            }
            forecast(latitude, longitude, (error, forecastData) => {
                if (error) {
                    return res.send({ error });
                }
                return res.send({
                    location,
                    forecast: forecastData
                });
            });
        });
    } else {
        return res.send({
            error: `Please provide location in search query....`
        });
    }
});

app.get('/products', (req, res) => {
    if (!(req.query.search)) {
        return res.send({
            error: `You must provide a search term`
        });
    }
    res.send({
        products: []
    })
});

app.get('/help/*', (req, res) => {
    res.render('error', {
        title: 'Error Ocuured.....',
        errMsg: 'Help article not found...'
    });
});

app.get('*', (req, res) => {
    res.render('error', {
        title: 'Error Ocuured.....',
        errMsg: 'Page not found...'
    })
});

app.listen(port, () => {
    log(`Server is up on port ${port}.....`);
});