const request = require('request');

const forecast = (latitude, longitude, callback) => {
    const url = `http://api.weatherstack.com/current?access_key=3716b8fe90c83039ea8d63ff13f026d4&query=${latitude},${longitude}&units=m`

    request({ url, json: true }, (error, { body }) => {
        if (error) {
            callback(`Unable to connect weather sevices`, undefined);
        } else if (body.error) {
            callback(`Unable to find related search`, undefined);
        } else {
            const { weather_descriptions, temperature, pressure, humidity } = body.current
            const data = {
                desc: weather_descriptions[0],
                temperature,
                pressure,
                humidity
            };
            callback(undefined, data);
        }
    });
}

module.exports = forecast;