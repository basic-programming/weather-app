const log = console.log;

log(`Client side Java script file is loaded...`);

const weatherForm = document.querySelector('form');
const search = document.querySelector('input');
const msg1 = document.querySelector('#msg1');
const msg2 = document.querySelector('#msg2');

weatherForm.addEventListener('submit', (e) => {
    e.preventDefault();
    msg1.innerHTML = 'Loading....';
    msg2.innerHTML = '';

    const location = search.value;
    fetch(`/weather?address=${location}`).then((response) => {
        response.json().then((data) => {
            if (data.error) {
                msg1.textContent = data.error;
                msg2.innerHTML = '';
            } else {
                const obj = data.forecast;
                msg1.innerHTML = '<hr>' + data.location;
                msg2.innerHTML = 'Its ' + obj.desc + ' today, temperature is ' + obj.temperature + ' deegre celsious, ' +
                    ' Pressure is ' + obj.pressure + ' bar Humidity in air is ' + obj.humidity + '% period....';
            }
        });
    });
});